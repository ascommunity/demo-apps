# Demo apps

Apps that demonstrate specific features available in AppSheet. 

Although there are many demo apps that have been built by the AppSheet team or by community members at large, there is not a good way to organize them such that users can find them easily and to learn more about how they were built.

This project is an attempt to improve the situation by collecting all demo apps, adding detailed documentation and tagging them for ease of search.

Contributions are welcome. Please register to get involved.

You can visit the [wiki](https://gitlab.com/ascommunity/demo-apps/-/wikis/home) to see more content.

### DISCLAIMER

This GitLab community is an online open-content collaborative site; that is, a voluntary association of individuals and groups working to develop a common resource of AppSheet knowledge. The structure of the project allows anyone with an Internet connection to alter its content. Please be advised that nothing found here has necessarily been reviewed by people with the expertise required to provide you with complete, accurate or reliable information.

That is not to say that you will not find valuable and accurate information here; much of the time you will. However, no entity, legal or otherwise, can guarantee the validity of the information found here. The content of any given article may recently have been changed, vandalized or altered by someone whose opinion does not correspond with the state of knowledge in the relevant fields. Note that most other reference works also have disclaimers.

#### No formal peer review

Our active community of editors uses tools to monitor new and changing content. However, this site is not uniformly peer reviewed; while readers may correct errors or engage in casual peer review, they have no legal duty to do so and thus all information read here is without any implied warranty of fitness for any purpose or use whatsoever. Even articles that have been vetted by informal peer review or featured article processes may later have been edited inappropriately, just before you view them.

None of the contributors, sponsors, administrators or anyone else connected with this site in any way whatsoever can be responsible for the appearance of any inaccurate or libelous information or for your use of the information contained in or linked from these web pages.

#### No contract; limited license

Please make sure that you understand that the information provided here is being provided freely, and that no kind of agreement or contract is created between you and the owners or users of this site, the owners of the servers upon which it is housed, the individual contributors, any project administrators, sysops or anyone else who is in any way connected with this project or sister projects subject to your claims against them directly. You are being granted a limited license to copy anything from this site; it does not create or imply any contractual or extracontractual liability on the part of the creator of this site or any of its agents, members, organizers or other users.

There is no agreement or understanding between you and the admins of this site regarding your use or modification of this information beyond the Creative Commons Attribution-Sharealike 3.0 Unported License (CC-BY-SA) and the GNU Free Documentation License (GFDL); neither is anyone at this site responsible should someone change, edit, modify or remove any information that you may post on this or any of its associated projects.

#### Trademarks

Any of the trademarks, service marks, collective marks, design rights or similar rights that are mentioned, used or cited in the articles in this site are the property of their respective owners. Their use here does not imply that you may use them for any purpose other than for the same or a similar informational use as contemplated by the original authors of these articles under the CC-BY-SA and GFDL licensing schemes. Unless otherwise stated this site is neither endorsed by nor affiliated with any of the holders of any such rights and as such no one can grant any rights to use any otherwise protected materials. Your use of any such or similar incorporeal property is at your own risk.

#### Jurisdiction and legality of content

Publication of information found here may be in violation of the laws of the country or jurisdiction from where you are viewing this information. Laws in your country or jurisdiction may not protect or allow the same kinds of speech or distribution. This site does not encourage the violation of any laws, and cannot be responsible for any violations of such laws, should you link to this domain or use, reproduce or republish the information contained herein.

#### Not professional advice

If you need specific advice (for example, medical, legal, financial or risk management), please seek a professional who is licensed or knowledgeable in that area. 
